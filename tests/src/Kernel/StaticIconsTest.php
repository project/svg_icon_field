<?php

namespace Drupal\Tests\svg_icon_field\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Tests the Static Icons class.
 */
class StaticIconsTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['system', 'file', 'svg_icon_field'];

  /**
   * Tests icons found.
   */
  public function testFindingIcons() {
    /** @var \Drupal\svg_icon_field\StaticIcons $static_icons */
    $static_icons = \Drupal::service('svg_icon_field.static_icons');
    $categories = $static_icons->getCategoriesStructure();
    $this->assertSame([
      'logos',
      'teamwork',
      'report',
      'delivery',
      'shopping_colored',
      'design',
      'design_colored',
      'eco_natural_colored',
      'xmas_set1',
      'food_colored_set1',
      'drink_food_colored',
      'allergy',
      'medicine_colored',
      'furniture_colored',
      'appliance_colored',
      'internet_thick',
      'communication_colored',
      'desktops_gadgets_colored',
      'emotions_colored',
      'filetype_content_colored',
      'sports_colored',
      'travel_transport',
      'arrows_colored',
      'weather_colored',
    ], array_keys($categories['categories']));

    // Test that the counts per directory match expectations. This is a fairly
    // safe way to know the retrieval of icons is as expected.
    /** @var \Drupal\Core\Extension\ExtensionPathResolver $path_resolver */
    $path_resolver = \Drupal::service('extension.path.resolver');
    $module_path = $path_resolver->getPath('module', 'svg_icon_field');
    $counts = [
      'icons/logos' => 28,
      'icons/teamwork' => 30,
      'icons/finance' => 100,
      'icons/delivery' => 35,
      'icons/shopping_colored' => 30,
      'icons/design' => 20,
      'icons/design_colored' => 40,
      'icons/eco_natural_colored' => 30,
      'icons/xmas' => 70,
      'icons/food_colored' => 39,
      'icons/drink_food_colored' => 90,
      'icons/allergy' => 36,
      'icons/medicine_colored' => 30,
      'icons/furniture_colored' => 29,
      'icons/appliance_colored' => 30,
      'icons/internet_thick' => 14,
      'icons/communication_colored' => 72,
      'icons/desktops_gadgets_colored' => 30,
      'icons/emotions_colored' => 50,
      'icons/filetype_content_colored' => 100,
      'icons/sports_colored' => 30,
      'icons/travel_transport' => 40,
      'icons/arrows_colored' => 50,
      'icons/weather_colored' => 30,
    ];
    $icons = [];
    foreach ($categories['categories'] as $category) {
      $icons = $static_icons->getIcons($module_path . '/' . $category['icons_path']);
      $this->assertCount($counts[$category['icons_path']], $icons);
    }

    // Validate that a single image is as expected. This will be the last image.
    // The path on gitlab will be different in both where module is stored
    // as well as module getting renamed. The most important things are that
    // each path starts with the base path of the site and ends with the file
    // extension.
    $example_value = reset($icons);
    $this->assertStringStartsWith('/modules/', $example_value);
    $this->assertStringContainsString('/svg_icon_field', $example_value);
    $this->assertStringContainsString('/icons/', $example_value);
    $this->assertStringEndsWith('.svg', $example_value);

    // The key should only contain the filename.
    $keys = array_keys($icons);
    $example_key = reset($keys);
    $this->assertStringNotContainsString('/', $example_key);
    $this->assertStringEndsWith('.svg', $example_key);
  }

}

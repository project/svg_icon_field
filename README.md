# SVG Icon Field

This module provides a new "SVG icon" field. By default it provides about 1000 categorized SVG icons. Icons used for project are released on CC0 license, that means it can be used for personal and commercial purposes without attribution.

For a full description of the module, visit the [project page](https://www.drupal.org/project/svg_icon_field).

Submit bug reports and feature suggestions, or track changes in the [issue queue](https://www.drupal.org/project/issues/svg_icon_field).


## Requirements
This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Go to entity manage fields page, add 'Icon' type field and click 'Continue'.
2. Provide a label for a field and select 'SVG icon' radio button.
3. Configure field by setting up help text, number of values, if the field is required and default value.
4. You can now use the field while adding / editing an entity.


## Maintainers
- Dawid Nawrot - [dawidnawrot](https://www.drupal.org/u/dawid_nawrot)
- Scott Euser - [scott_euser](https://www.drupal.org/u/scott_euser)

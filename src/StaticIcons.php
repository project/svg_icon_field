<?php

namespace Drupal\svg_icon_field;

use Drupal\Component\Serialization\Yaml;
use Drupal\Core\Extension\ExtensionPathResolver;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Icons class.
 */
class StaticIcons {

  use StringTranslationTrait;

  /**
   * Constructs a new StaticIcons object.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler service.
   * @param \Drupal\Core\Extension\ExtensionPathResolver $extensionPathResolver
   *   The extension path resolver.
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   The file system.
   */
  public function __construct(
    protected ModuleHandlerInterface $moduleHandler,
    protected ExtensionPathResolver $extensionPathResolver,
    protected FileSystemInterface $fileSystem,
  ) {
  }

  /**
   * Get the category for given id or all.
   *
   * @param string $id
   *   ID of the category.
   *
   * @return array
   *   Array of categories or single category.
   */
  public function getCategoriesStructure($id = NULL) {
    $path = $this->extensionPathResolver->getPath('module', 'svg_icon_field');
    $categories = Yaml::decode(file_get_contents($path . '/icons/icons.categories.yaml'));
    $this->moduleHandler->invokeAll('svg_icon_field_categories_alter', [&$categories]);
    return (!empty($id)) ? $categories['categories'][$id] : $categories;
  }

  /**
   * Get icons by directory or return all if dir is null.
   *
   * @param string $path
   *   Path to directory with icons.
   *
   * @return array
   *   Array of options to feed an icon field.
   */
  public function getIcons(string $path): array {
    global $base_path;
    $files = $this->fileSystem->scanDirectory($path, '/.*\.*$/');

    $results = [];
    foreach ($files as $file) {
      if (!is_object($file) || empty($file->filename) || empty($file->uri)) {
        continue;
      }
      $results[$file->filename] = $base_path . $file->uri;
    }
    return $results;
  }

  /**
   * Get dir path to category.
   *
   * @param string $category
   *   ID of the category.
   *
   * @return string
   *   Path to directory where icons are stored.
   */
  public function getCategoryLocation($category) {
    $locations = $this->getCategoriesStructure();
    $type = $locations['categories'][$category]['element_type'];
    $name = $locations['categories'][$category]['element_name'];
    $path = $locations['categories'][$category]['icons_path'];
    $path_base = $this->extensionPathResolver->getPath($type, $name);
    return $path_base . '/' . $path;
  }

  /**
   * Get category attribution.
   *
   * Sometimes there are icons which requires crediting of an author.
   * That's an attribution. This function is going to get it.
   *
   * @param string $category
   *   ID of the category.
   *
   * @return string
   *   String or html of the attribution.
   */
  public function getCategoryAttribution($category) {
    // Get categories with its labels.
    $categories = $this->getCategoriesStructure();
    return $categories['categories'][$category]['attribution'];
  }

  /**
   * Get human readable icon categories based on yaml file.
   *
   * @return array
   *   Complete array of categories.
   */
  public function getHumanReadableIconCategories() {
    $category_options = [];

    // Get categories with its labels.
    $categories = $this->getCategoriesStructure();

    $categories = $categories['categories'];
    foreach ($categories as $category_key => $category_value) {
      $category_options[(string) $category_value['group']][$category_key] = $category_value['label'];
    }

    return $category_options;
  }

}

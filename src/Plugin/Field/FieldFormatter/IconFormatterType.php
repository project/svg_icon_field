<?php

namespace Drupal\svg_icon_field\Plugin\Field\FieldFormatter;

use Drupal\Core\Extension\ExtensionPathResolver;
use Drupal\Core\Field\Attribute\FieldFormatter;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\svg_icon_field\StaticIcons;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides an icon field formatter.
 */
#[FieldFormatter(
  id: 'icon_formatter_type',
  label: new TranslatableMarkup('Icon formatter type'),
  field_types: [
    'icon_field_type',
  ],
)]
class IconFormatterType extends FormatterBase implements ContainerFactoryPluginInterface {

  /**
   * Constructs a FormatterBase object.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\svg_icon_field\StaticIcons $staticIcons
   *   The static icons service.
   * @param \Drupal\Core\Extension\ExtensionPathResolver $extensionPathResolver
   *   The extension path resolver.
   */
  public function __construct(
    $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    $label,
    $view_mode,
    array $third_party_settings,
    protected StaticIcons $staticIcons,
    protected ExtensionPathResolver $extensionPathResolver,
  ) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('svg_icon_field.static_icons'),
      $container->get('extension.path.resolver'),
    );
  }

  /**
   * {@inheritdoc}
   *
   * That's default settings for the form defined in settingsForm.
   */
  public static function defaultSettings() {
    return [
      // Implement default settings.
      'width' => 100,
      'height' => 100,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   *
   * These settings appears on `manage display` tab on node / entity edit form
   * Sample path for `test` node type: admin/structure/types/manage/test/display
   * next to field you need to click cog icon to get to the form.
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);

    $form['width'] = [
      '#type' => 'number',
      '#title' => $this->t('Image width'),
      '#default_value' => $this->getSetting('width'),
    ];

    $form['height'] = [
      '#type' => 'number',
      '#title' => $this->t('Image height'),
      '#default_value' => $this->getSetting('height'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   *
   * This summary appears in on `manage display` tab on node / entity edit form.
   * Example path for `test` node type admin/structure/types/manage/test/display
   * You can find it in unnamed column,
   * here is the screenshot: https://imgur.com/T4F38uq .
   */
  public function settingsSummary() {
    $summary = [];
    // Implement settings summary.
    $summary[] = $this->t('Image width:') . ' ' . $this->getSetting('width');
    $summary[] = $this->t('Image height:') . ' ' . $this->getSetting('height');
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    global $base_path;
    foreach ($items as $delta => $item) {

      $category = $this->staticIcons->getCategoriesStructure($item->getValue()['group']);

      $path = $this->extensionPathResolver->getPath($category['element_type'], $category['element_name']);
      $uri = $base_path . $path . '/' . $category['icons_path'] . '/' . $item->getValue()['icon'];
      $elements[$delta] = [
        '#theme' => 'svg_icon_formatter',
        '#uri' => $uri,
        '#width' => $this->getSetting('width'),
        '#height' => $this->getSetting('height'),
      ];
    }

    return $elements;
  }

}
